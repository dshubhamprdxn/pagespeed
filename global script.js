var pageSpeedApiKey = 'AIzaSyDqEGvYdQbMC_i7ihHlJYtr5yUSdJ5ePfY';
var desktop,mobile,spreadsheet,sheet, urlSpreadSheet, urlSheet, urls;
var pageSpeedMonitorUrls = [];
var pageSpeedMonitorPages = [];
var pageSpeedMonitorWebsites = [];
var performanceOpportunities = ['efficient-animated-content', 
                                'render-blocking-resources',
                                'uses-optimized-images',
                                'uses-text-compression',
                                'uses-rel-preconnect',
                                'unminified-css',
                                'offscreen-images',
                                'uses-responsive-images',
                                'unused-css-rules',
                                'uses-webp-images',
                                'uses-rel-preload',
                                'unminified-javascript',
                                'redirects',                                
                               ];

var seoOpportunities = ['document-title',
                        'robots-txt', 
                        'link-text', 
                        'http-status-code',
                        'image-alt', 
                        'plugins',
                        'meta-description',
                        'viewport', 
                        'canonical', 
                        'hreflang', 
                        'is-crawlable'                                                                         
                       ]; 

var accOpportunities = ['color-contrast', 
                        'bypass', 
                        'image-alt',                        
                        'html-has-lang',
                        'link-name',
                        'meta-viewport'
                       ];

var bestPractOpportunities = ['appcache-manifest',
                              'external-anchors-use-rel-noopener',
                              'deprecations',
                              'no-vulnerable-libraries',
                              'password-inputs-can-be-pasted-into',
                              'doctype',
                              'errors-in-console',
                              'js-libraries',
                              'geolocation-on-start',
                              'notification-on-start',
                              'is-on-https',
                              'no-document-write',
                              'image-aspect-ratio',
                              'uses-passive-event-listeners'
];

var newAuditDate,i,j,ranges; //i,j are set to global for looping from function to function

function monitorHeterodox() {
  urlSpreadSheet = SpreadsheetApp.getActiveSpreadsheet();
  urlSheet = urlSpreadSheet.getSheetByName('Heterodox - Site Urls');
  urls = urlSheet.getDataRange().getValues();
  getSiteUrlsData();
  
  var spreadsheet = SpreadsheetApp.getActiveSpreadsheet();
    
  oSheet = spreadsheet.getSheetByName('Heterodox - Opportunities');
  var lastRow = oSheet.getLastRow();
  oSheet.getRange("A2:H"+lastRow).clear();

  for (i = 0; i < pageSpeedMonitorUrls.length; i++) {          
    desktop = callPageSpeed('desktop', pageSpeedMonitorUrls[i]);
    mobile = callPageSpeed('mobile', pageSpeedMonitorUrls[i]);
    
    if (Object.keys(desktop).length === 0 && Object.keys(mobile).length === 0 ) {
      continue;
    }

    ranges = spreadsheet.getSheetByName('Vitals Ranges');
    if (i === 0) {
      coreVitalRange();
    }
//  throw new Error("Something went badly wrong!");
    sheet = spreadsheet.getSheetByName('Heterodox - Results');
    appendRows();
  }
}

function monitorPRDXN() {
  urlSpreadSheet = SpreadsheetApp.getActiveSpreadsheet();
  urlSheet = urlSpreadSheet.getSheetByName('PRDXN - Site Urls');
  urls = urlSheet.getDataRange().getValues();
  getSiteUrlsData();
  
  var spreadsheet = SpreadsheetApp.getActiveSpreadsheet();
    
  oSheet = spreadsheet.getSheetByName('PRDXN - Opportunities');
  var lastRow = oSheet.getLastRow();
  console.log(lastRow);
  oSheet.getRange("A2:H"+lastRow).clear();

  for (i = 0; i < pageSpeedMonitorUrls.length; i++) {          
    desktop = callPageSpeed('desktop', pageSpeedMonitorUrls[i]);
    mobile = callPageSpeed('mobile', pageSpeedMonitorUrls[i]);
    
    if (Object.keys(desktop).length === 0 && Object.keys(mobile).length === 0 ) {
      continue;
    }
    
    sheet = spreadsheet.getSheetByName('PRDXN - Results');
    appendRows();
  }
}

function monitorTheEconomistGroup() {
  urlSpreadSheet = SpreadsheetApp.getActiveSpreadsheet();
  urlSheet = urlSpreadSheet.getSheetByName('The Economist Group - Site Urls');
  urls = urlSheet.getDataRange().getValues();
  
  getSiteUrlsData();
  
  var spreadsheet = SpreadsheetApp.getActiveSpreadsheet();
    
  oSheet = spreadsheet.getSheetByName('The Economist Group - Opportunities');
  var lastRow = oSheet.getLastRow();
  oSheet.getRange("A2:H"+lastRow).clear();

  for (i = 0; i < pageSpeedMonitorUrls.length; i++) {          
    desktop = callPageSpeed('desktop', pageSpeedMonitorUrls[i]);
    mobile = callPageSpeed('mobile', pageSpeedMonitorUrls[i]);
    
    if (Object.keys(desktop).length === 0 && Object.keys(mobile).length === 0 ) {
      continue;
    }
    
    sheet = spreadsheet.getSheetByName('The Economist Group - Results');
    appendRows();
  }
}

function getSiteUrlsData() {
  
  function addDays(date, days) {
    const copy = new Date(Number(date))
    copy.setDate(date.getDate() + days)
    return copy;
  }

  const date = new Date();
  newAuditDate = addDays(date, 14);       
 
  for (var x = 1; x < urls.length; x++) {
    pageSpeedMonitorUrls.push(urls[x][0]);
    pageSpeedMonitorPages.push(urls[x][1]);
    pageSpeedMonitorWebsites.push(urls[x][2]); 
  }
}

function coreVitalRange() {
    var lastRow = ranges.getLastRow();
    ranges.getRange("A2:E"+lastRow).clear();
    var threesholdCategory = ['FAST','AVERAGE','SLOW'];
    var vitals = ['FIRST_INPUT_DELAY_MS','CUMULATIVE_LAYOUT_SHIFT_SCORE','LARGEST_CONTENTFUL_PAINT_MS'];
    for(var l = 0; l < 3; l++) {
      for (var k = 0; k < 3; k++) {
        if (l !== 1) { // for CLS division is required
          let max = desktop['loadingExperience']['metrics'][vitals[l]]['distributions'][k]['max'];
          if (max) {
            max = max/1000;
          }
          ranges.appendRow([
            vitals[l],
            threesholdCategory[k],
            desktop['loadingExperience']['metrics'][vitals[l]]['distributions'][k]['min']/1000,
            max,
            desktop['loadingExperience']['metrics'][vitals[l]]['distributions'][k]['proportion']
          ]);
        } else {
//          let max = desktop['loadingExperience']['metrics'][vitals[l]]['distributions'][k]['max'];
//          if (max) {
//            max = max/100
//          }
          ranges.appendRow([
            vitals[l],
            threesholdCategory[k],
            desktop['loadingExperience']['metrics'][vitals[l]]['distributions'][k]['min'],
            desktop['loadingExperience']['metrics'][vitals[l]]['distributions'][k]['max'],
            desktop['loadingExperience']['metrics'][vitals[l]]['distributions'][k]['proportion']
          ]);
        }
      }
    }
  }

function appendRows() {
  if (desktop['loadingExperience']['metrics']) {
  sheet.appendRow([
    Utilities.formatDate(new Date(), 'GMT', 'yyyy-MM-dd'),                   
    pageSpeedMonitorWebsites[i],             
    pageSpeedMonitorPages[i],
    pageSpeedMonitorUrls[i],
    'Desktop',        
    desktop['lighthouseResult']['audits']['metrics']['details']['items'][0]['speedIndex']/1000,
    desktop['lighthouseResult']['audits']['metrics']['details']['items'][0]['firstContentfulPaint']/1000,                   
    desktop['lighthouseResult']['audits']['metrics']['details']['items'][0]['firstMeaningfulPaint']/1000,                   
    desktop['lighthouseResult']['audits']['metrics']['details']['items'][0]['interactive']/1000,                   
    desktop['lighthouseResult']['audits']['metrics']['details']['items'][0]['firstCPUIdle']/1000,
    desktop['lighthouseResult']['audits']['metrics']['details']['items'][0]['estimatedInputLatency']/1000,
    desktop['lighthouseResult']['audits']['server-response-time']['numericValue'].toFixed(0)/1000,
//    desktop['loadingExperience']['metrics']['FIRST_INPUT_DELAY_MS']['category'],
    desktop['loadingExperience']['metrics']['FIRST_INPUT_DELAY_MS']['percentile']/1000,
//    desktop['loadingExperience']['metrics']['CUMULATIVE_LAYOUT_SHIFT_SCORE']['category'],
      desktop['loadingExperience']['metrics']['CUMULATIVE_LAYOUT_SHIFT_SCORE']['percentile'], // CLS is unitlessvalues
//    desktop['loadingExperience']['metrics']['LARGEST_CONTENTFUL_PAINT_MS']['category'],
    desktop['loadingExperience']['metrics']['LARGEST_CONTENTFUL_PAINT_MS']['percentile']/1000,
    desktop['lighthouseResult']['categories']['performance']['score'] * 100,
    desktop['lighthouseResult']['categories']['accessibility']['score'] * 100,
    desktop['lighthouseResult']['categories']['best-practices']['score'] * 100,
    desktop['lighthouseResult']['categories']['seo']['score'] * 100,
    Utilities.formatDate(newAuditDate, 'GMT', 'yyyy-MM-dd')
  ]);
  } else {
    sheet.appendRow([
    Utilities.formatDate(new Date(), 'GMT', 'yyyy-MM-dd'),                   
    pageSpeedMonitorWebsites[i],             
    pageSpeedMonitorPages[i],
    pageSpeedMonitorUrls[i],
    'Desktop',        
    desktop['lighthouseResult']['audits']['metrics']['details']['items'][0]['speedIndex']/1000,
    desktop['lighthouseResult']['audits']['metrics']['details']['items'][0]['firstContentfulPaint']/1000,                   
    desktop['lighthouseResult']['audits']['metrics']['details']['items'][0]['firstMeaningfulPaint']/1000,                   
    desktop['lighthouseResult']['audits']['metrics']['details']['items'][0]['interactive']/1000,                   
    desktop['lighthouseResult']['audits']['metrics']['details']['items'][0]['firstCPUIdle']/1000,
    desktop['lighthouseResult']['audits']['metrics']['details']['items'][0]['estimatedInputLatency']/1000,
    desktop['lighthouseResult']['audits']['server-response-time']['numericValue'].toFixed(0)/1000,
    '',
    '',
    '',
    desktop['lighthouseResult']['categories']['performance']['score'] * 100,
    desktop['lighthouseResult']['categories']['accessibility']['score'] * 100,
    desktop['lighthouseResult']['categories']['best-practices']['score'] * 100,
    desktop['lighthouseResult']['categories']['seo']['score'] * 100,
    Utilities.formatDate(newAuditDate, 'GMT', 'yyyy-MM-dd')
    ]);
  }
  
  if (mobile['loadingExperience']['metrics']) {
  sheet.appendRow([
    Utilities.formatDate(new Date(), 'GMT', 'yyyy-MM-dd'),                   
    pageSpeedMonitorWebsites[i],             
    pageSpeedMonitorPages[i],
    pageSpeedMonitorUrls[i],    
    'Mobile',                   
    mobile['lighthouseResult']['audits']['metrics']['details']['items'][0]['speedIndex']/1000,
    mobile['lighthouseResult']['audits']['metrics']['details']['items'][0]['firstContentfulPaint']/1000,                   
    mobile['lighthouseResult']['audits']['metrics']['details']['items'][0]['interactive']/1000,
    mobile['lighthouseResult']['audits']['metrics']['details']['items'][0]['firstMeaningfulPaint']/1000,
    mobile['lighthouseResult']['audits']['metrics']['details']['items'][0]['firstCPUIdle']/1000,
    mobile['lighthouseResult']['audits']['metrics']['details']['items'][0]['estimatedInputLatency']/1000,
    mobile['lighthouseResult']['audits']['server-response-time']['numericValue'].toFixed(0)/1000,
//    mobile['loadingExperience']['metrics']['FIRST_INPUT_DELAY_MS']['category'],
    mobile['loadingExperience']['metrics']['FIRST_INPUT_DELAY_MS']['percentile']/1000,
//    mobile['loadingExperience']['metrics']['CUMULATIVE_LAYOUT_SHIFT_SCORE']['category'],
    mobile['loadingExperience']['metrics']['CUMULATIVE_LAYOUT_SHIFT_SCORE']['percentile'],
//    mobile['loadingExperience']['metrics']['LARGEST_CONTENTFUL_PAINT_MS']['category'],
    mobile['loadingExperience']['metrics']['LARGEST_CONTENTFUL_PAINT_MS']['percentile']/1000,
    mobile['lighthouseResult']['categories']['performance']['score'] * 100,
    mobile['lighthouseResult']['categories']['accessibility']['score'] * 100,
    mobile['lighthouseResult']['categories']['best-practices']['score'] * 100,
    mobile['lighthouseResult']['categories']['seo']['score'] * 100,
    Utilities.formatDate(newAuditDate, 'GMT', 'yyyy-MM-dd')
  ]);  
  } else {
    sheet.appendRow([
    Utilities.formatDate(new Date(), 'GMT', 'yyyy-MM-dd'),                   
    pageSpeedMonitorWebsites[i],             
    pageSpeedMonitorPages[i],
    pageSpeedMonitorUrls[i],    
    'Mobile',                   
    mobile['lighthouseResult']['audits']['metrics']['details']['items'][0]['speedIndex']/1000,
    mobile['lighthouseResult']['audits']['metrics']['details']['items'][0]['firstContentfulPaint']/1000,                   
    mobile['lighthouseResult']['audits']['metrics']['details']['items'][0]['interactive']/1000,
    mobile['lighthouseResult']['audits']['metrics']['details']['items'][0]['firstMeaningfulPaint']/1000,
    mobile['lighthouseResult']['audits']['metrics']['details']['items'][0]['firstCPUIdle']/1000,
    mobile['lighthouseResult']['audits']['metrics']['details']['items'][0]['estimatedInputLatency']/1000,
    mobile['lighthouseResult']['audits']['server-response-time']['numericValue'].toFixed(0)/1000,
    '',
    '',
    '',
    mobile['lighthouseResult']['categories']['performance']['score'] * 100,
    mobile['lighthouseResult']['categories']['accessibility']['score'] * 100,
    mobile['lighthouseResult']['categories']['best-practices']['score'] * 100,
    mobile['lighthouseResult']['categories']['seo']['score'] * 100,
    Utilities.formatDate(newAuditDate, 'GMT', 'yyyy-MM-dd')
   ]);
  } 
  
  for(var j = 0; j < performanceOpportunities.length; j++) {
    if(desktop['lighthouseResult']['audits'][performanceOpportunities[j]]['score'] < 1) {
//      console.log("klkl");
      oSheet.appendRow([      
        Utilities.formatDate(new Date(), 'GMT', 'yyyy-MM-dd'),                   
        pageSpeedMonitorWebsites[i],             
        pageSpeedMonitorPages[i],
        pageSpeedMonitorUrls[i],    
        'Desktop',      
        'Performance',
        desktop['lighthouseResult']['audits'][performanceOpportunities[j]]['title'],   
        desktop['lighthouseResult']['audits'][performanceOpportunities[j]]['score'],      
      ]); 
    }
        
    if(mobile['lighthouseResult']['audits'][performanceOpportunities[j]]['score'] < 1) {  
      oSheet.appendRow([      
        Utilities.formatDate(new Date(), 'GMT', 'yyyy-MM-dd'),                   
        pageSpeedMonitorWebsites[i],             
        pageSpeedMonitorPages[i],
        pageSpeedMonitorUrls[i],    
        'Mobile',      
        'Performance',
        mobile['lighthouseResult']['audits'][performanceOpportunities[j]]['title'],   
        mobile['lighthouseResult']['audits'][performanceOpportunities[j]]['score'],      
      ]);
    }
  }
        
//  throw new Error("Something went badly wrong!");     
  for(var j = 0; j < seoOpportunities.length; j++) {
    if(desktop['lighthouseResult']['audits'][seoOpportunities[j]]['score'] < 1) {
      oSheet.appendRow([      
        Utilities.formatDate(new Date(), 'GMT', 'yyyy-MM-dd'),                   
        pageSpeedMonitorWebsites[i],             
        pageSpeedMonitorPages[i],
        pageSpeedMonitorUrls[i],    
        'Desktop',      
        'Seo',
        desktop['lighthouseResult']['audits'][seoOpportunities[j]]['title'],   
        desktop['lighthouseResult']['audits'][seoOpportunities[j]]['score'],      
      ]); 
    }

    if(mobile['lighthouseResult']['audits'][seoOpportunities[j]]['score'] < 1) {    
      oSheet.appendRow([      
        Utilities.formatDate(new Date(), 'GMT', 'yyyy-MM-dd'),                        
        pageSpeedMonitorWebsites[i],             
        pageSpeedMonitorPages[i],
        pageSpeedMonitorUrls[i],    
        'Mobile',      
        'Seo',
        mobile['lighthouseResult']['audits'][seoOpportunities[j]]['title'],   
        mobile['lighthouseResult']['audits'][seoOpportunities[j]]['score'],      
      ]);
    }
  }

//   throw new Error("Something went badly wrong!"); 
  for(var j = 0; j < accOpportunities.length; j++) {
    if (desktop['lighthouseResult']['audits'][accOpportunities[j]]['score'] < 1) {  
      oSheet.appendRow([      
        Utilities.formatDate(new Date(), 'GMT', 'yyyy-MM-dd'),                   
        pageSpeedMonitorWebsites[i],             
        pageSpeedMonitorPages[i],
        pageSpeedMonitorUrls[i],    
        'Desktop',      
        'Accessibility',
        desktop['lighthouseResult']['audits'][accOpportunities[j]]['title'],   
        desktop['lighthouseResult']['audits'][accOpportunities[j]]['score'],      
      ]); 
    }
    if(mobile['lighthouseResult']['audits'][accOpportunities[j]]['score'] < 1) {  
      oSheet.appendRow([      
        Utilities.formatDate(new Date(), 'GMT', 'yyyy-MM-dd'),                   
        pageSpeedMonitorWebsites[i],             
        pageSpeedMonitorPages[i],
        pageSpeedMonitorUrls[i],    
        'Mobile',      
        'Accessibility',
        mobile['lighthouseResult']['audits'][accOpportunities[j]]['title'],   
        mobile['lighthouseResult']['audits'][accOpportunities[j]]['score'],      
      ]);
    }
  }

//        throw new Error("Something went badly wrong!");
  for(var j = 0; j < bestPractOpportunities.length; j++) {
    if(desktop['lighthouseResult']['audits'][bestPractOpportunities[j]]['score'] < 1) {
      oSheet.appendRow([      
        Utilities.formatDate(new Date(), 'GMT', 'yyyy-MM-dd'),                      
        pageSpeedMonitorWebsites[i],             
        pageSpeedMonitorPages[i],
        pageSpeedMonitorUrls[i],    
        'Desktop',      
        'Best-practices',
        desktop['lighthouseResult']['audits'][bestPractOpportunities[j]]['title'],   
        desktop['lighthouseResult']['audits'][bestPractOpportunities[j]]['score'],      
      ]); 
    }

    if(mobile['lighthouseResult']['audits'][bestPractOpportunities[j]]['score'] < 1) {       
      oSheet.appendRow([      
        Utilities.formatDate(new Date(), 'GMT', 'yyyy-MM-dd'),                   
        pageSpeedMonitorWebsites[i],             
        pageSpeedMonitorPages[i],
        pageSpeedMonitorUrls[i],    
        'Mobile',      
        'Best-practices',
        mobile['lighthouseResult']['audits'][bestPractOpportunities[j]]['title'],   
        mobile['lighthouseResult']['audits'][bestPractOpportunities[j]]['score'],      
      ]);
    }
  }
}
        
function callPageSpeed(strategy, url) {
  //var pageSpeedUrl = 'https://www.googleapis.com/pagespeedonline/v5/runPagespeed?url=' + url + '&key=' + pageSpeedApiKey + '&category=seo' +'&strategy=' + strategy;
  var pageSpeedUrl = 'https://www.googleapis.com/pagespeedonline/v5/runPagespeed?url=' + url + '&key=' + pageSpeedApiKey + '&category=performance' + '&category=seo' + '&category=accessibility' + '&category=best-practices' +'&strategy=' + strategy;
//  var pageSpeedUrl = 'https://www.googleapis.com/pagespeedonline/v5/runPagespeed?url=' + url + '&key=' + pageSpeedApiKey + '&category=performance&strategy=' + strategy;               
  
                 options = {muteHttpExceptions: true};
  var response = UrlFetchApp.fetch(pageSpeedUrl, options);
  var json = response.getContentText();

  if(response.getResponseCode() == "200") {
    return JSON.parse(json);   
  } else {
    return response.getResponseCode();
    MailApp.sendEmail("nikhils@prdxn.com",                                         
                  "Page Speed App - Problem URL",
                  "Can`t get Page Speed for"+ url +". Response from server is" + response.getResponseCode());
  }    
}