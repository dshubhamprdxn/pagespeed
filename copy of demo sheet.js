var pageSpeedApiKey = 'AIzaSyDqEGvYdQbMC_i7ihHlJYtr5yUSdJ5ePfY';
var desktop,mobile,spreadsheet,sheet, urlSpreadSheet, urlSheet, urls, getSheetRows;
var pageSpeedMonitorUrls = [];
var pageSpeedMonitorPages = [];
var pageSpeedMonitorWebsites = [];
var performanceOpportunities = ['efficient-animated-content', 
                                'render-blocking-resources',
                                'uses-optimized-images',
                                'uses-text-compression',
                                'uses-rel-preconnect',
                                'unminified-css',
                                'offscreen-images',
                                'uses-responsive-images',
                                'unused-css-rules',
                                'uses-webp-images',
                                'uses-rel-preload',
                                'unminified-javascript',
                                'redirects',                                
                               ];

var seoOpportunities = ['document-title',
                        'robots-txt', 
                        'link-text', 
                        'http-status-code',
                        'image-alt', 
                        'plugins',
                        'meta-description',
                        'viewport', 
                        'canonical', 
                        'hreflang', 
                        'is-crawlable'                                                                         
                       ]; 

var accOpportunities = ['color-contrast', 
                        'bypass', 
                        'image-alt',                        
                        'html-has-lang',
                        'link-name',
                        'meta-viewport'
                       ];

var bestPractOpportunities = ['appcache-manifest',
                              'external-anchors-use-rel-noopener',
                              'deprecations',
                              'no-vulnerable-libraries',
                              'password-inputs-can-be-pasted-into',
                              'doctype',
                              'errors-in-console',
                              'js-libraries',
                              'geolocation-on-start',
                              'notification-on-start',
                              'is-on-https',
                              'no-document-write',
                              'image-aspect-ratio',
                              'uses-passive-event-listeners'
];

var client,newAuditDate,i,j,ranges; //i,j are set to global for looping from function to function
var failed = [];
var count = 1;
var failedValues = [];
var failedScripts = [];
var failedScriptMessages = [];

function monitorHeterodox() {
  client = 'Heterodox';
  urlSpreadSheet = SpreadsheetApp.getActiveSpreadsheet();
  urlSheet = urlSpreadSheet.getSheetByName('Heterodox - Site Urls');
  urls = urlSheet.getDataRange().getValues();
  getSiteUrlsData();
  
  var spreadsheet = SpreadsheetApp.getActiveSpreadsheet();
    
  oSheet = spreadsheet.getSheetByName('Heterodox - Opportunities');
  var lastRow = oSheet.getLastRow();
  oSheet.getRange("A2:H"+lastRow).clear();

  for (i = 0; i < pageSpeedMonitorUrls.length; i++) {          
    desktop = callPageSpeed('desktop', pageSpeedMonitorUrls[i]);
    mobile = callPageSpeed('mobile', pageSpeedMonitorUrls[i]);
    
    if (Object.keys(desktop).length === 0 && Object.keys(mobile).length === 0 ) {
      continue;
    }

    ranges = spreadsheet.getSheetByName('Vitals Ranges');
    if (i === 0) {
      coreVitalRange();
    }
//  throw new Error("Something went badly wrong!");
    sheet = spreadsheet.getSheetByName('Heterodox - Results');
    appendRows(i);
  }
  if(failedValues.length > 0) {
    testUrlsAgain();
  }
 sendMail('shubhamd@prdxn.com');
//  if (failed.length > 0) {
//    var emailAddress = 'shubhamd@prdxn.com';
//    var subject = 'Sending emails as the url failed for 5 times in fetching data for speedtest';
////    failed;
//    MailApp.sendEmail(emailAddress, subject, failed);
//  }
}

function monitorPRDXN() {
  client = 'PRDXN';
  urlSpreadSheet = SpreadsheetApp.getActiveSpreadsheet();
  urlSheet = urlSpreadSheet.getSheetByName('PRDXN - Site Urls');
  urls = urlSheet.getDataRange().getValues();
  getSiteUrlsData();
  
  var spreadsheet = SpreadsheetApp.getActiveSpreadsheet();
    
  oSheet = spreadsheet.getSheetByName('PRDXN - Opportunities');
  var lastRow = oSheet.getLastRow();
  oSheet.getRange("A2:H"+lastRow).clear();

  for (i = 0; i < pageSpeedMonitorUrls.length; i++) {          
    desktop = callPageSpeed('desktop', pageSpeedMonitorUrls[i]);
    mobile = callPageSpeed('mobile', pageSpeedMonitorUrls[i]);
    
    if (Object.keys(desktop).length === 0 && Object.keys(mobile).length === 0 ) {
      continue;
    }
//    throw new Exception("something went wrong");
    sheet = spreadsheet.getSheetByName('PRDXN - Results');
    appendRows(i);
  }
  if(failedValues.length > 0) {
    testUrlsAgain();
  }
 sendMail('shubhamd@prdxn.com');
}

function monitorTheEconomistGroup() {
  client = 'The Economist Group';
  urlSpreadSheet = SpreadsheetApp.getActiveSpreadsheet();
  urlSheet = urlSpreadSheet.getSheetByName('The Economist Group - Site Urls');
//  urlSheet = urlSpreadSheet.getSheetByName('Sheet9');
  urls = urlSheet.getDataRange().getValues();
  
  getSiteUrlsData();
  
  var spreadsheet = SpreadsheetApp.getActiveSpreadsheet();
//    
  oSheet = spreadsheet.getSheetByName('The Economist Group - Opportunities');
  var lastRow = oSheet.getLastRow();
  oSheet.getRange("A2:H"+lastRow).clear();

  for (i = 0; i < pageSpeedMonitorUrls.length; i++) {          
    desktop = callPageSpeed('desktop', pageSpeedMonitorUrls[i]);
    mobile = callPageSpeed('mobile', pageSpeedMonitorUrls[i]);
    
    if (Object.keys(desktop).length === 0 && Object.keys(mobile).length === 0 ) {
      continue;
    }
    
    sheet = spreadsheet.getSheetByName('The Economist Group - Results');
       appendRows(i);
  }
  if(failedValues.length > 0) {
    testUrlsAgain();
  }
 sendMail('shubhamd@prdxn.com');
}

function sendMail(email) {
  if (failedScriptMessages.length > 0) {
    var emailAddress = email;
    var subject = 'Urls failed for fetching data for pagespeed insight test';
    MailApp.sendEmail(emailAddress, subject, failedScriptMessages);
  }
}

function getSiteUrlsData() {
  
  function addDays(date, days) {
    const copy = new Date(Number(date))
    copy.setDate(date.getDate() + days)
    return copy;
  }

  const date = new Date();
  newAuditDate = addDays(date, 14);       
 
  for (var x = 1; x < urls.length; x++) {
    pageSpeedMonitorUrls.push(urls[x][0]);
    pageSpeedMonitorPages.push(urls[x][1]);
    pageSpeedMonitorWebsites.push(urls[x][2]); 
  }
}

function coreVitalRange() {
    var lastRow = ranges.getLastRow();
    ranges.getRange("A2:E"+lastRow).clear();
    var threesholdCategory = ['FAST','AVERAGE','SLOW'];
    var vitals = ['FIRST_INPUT_DELAY_MS','CUMULATIVE_LAYOUT_SHIFT_SCORE','LARGEST_CONTENTFUL_PAINT_MS'];
    for(var l = 0; l < 3; l++) {
      for (var k = 0; k < 3; k++) {
        if (l !== 1) { // for CLS division is required
          let max = desktop['loadingExperience']['metrics'][vitals[l]]['distributions'][k]['max'];
          if (max) {
            max = max/1000;
          }
          ranges.appendRow([
            vitals[l],
            threesholdCategory[k],
            desktop['loadingExperience']['metrics'][vitals[l]]['distributions'][k]['min']/1000,
            max,
            desktop['loadingExperience']['metrics'][vitals[l]]['distributions'][k]['proportion']
          ]);
        } else {
//          let max = desktop['loadingExperience']['metrics'][vitals[l]]['distributions'][k]['max'];
//          if (max) {
//            max = max/100
//          }
          ranges.appendRow([
            vitals[l],
            threesholdCategory[k],
            desktop['loadingExperience']['metrics'][vitals[l]]['distributions'][k]['min'],
            desktop['loadingExperience']['metrics'][vitals[l]]['distributions'][k]['max'],
            desktop['loadingExperience']['metrics'][vitals[l]]['distributions'][k]['proportion']
          ]);
        }
      }
    }
  }

function appendRows(iterate) {
    
  try {
    i = iterate;
    // var desktopData = new Promise( (respon) => {})
    if (desktop['loadingExperience'] !== undefined && desktop['loadingExperience']['metrics']) {
    sheet.appendRow([
        Utilities.formatDate(new Date(), 'GMT', 'yyyy-MM-dd'),                   
        pageSpeedMonitorWebsites[i],             
        pageSpeedMonitorPages[i],
        pageSpeedMonitorUrls[i],
        'Desktop',        
        desktop['lighthouseResult']['audits']['metrics']['details']['items'][0]['speedIndex']/1000,
        desktop['lighthouseResult']['audits']['metrics']['details']['items'][0]['firstContentfulPaint']/1000,                   
        desktop['lighthouseResult']['audits']['metrics']['details']['items'][0]['firstMeaningfulPaint']/1000,                   
        desktop['lighthouseResult']['audits']['metrics']['details']['items'][0]['interactive']/1000,                   
        desktop['lighthouseResult']['audits']['metrics']['details']['items'][0]['firstCPUIdle']/1000,
        desktop['lighthouseResult']['audits']['metrics']['details']['items'][0]['estimatedInputLatency']/1000,
        desktop['lighthouseResult']['audits']['server-response-time']['numericValue'].toFixed(0)/1000,
    //    desktop['loadingExperience']['metrics']['FIRST_INPUT_DELAY_MS']['category'],
        desktop['loadingExperience']['metrics']['FIRST_INPUT_DELAY_MS']['percentile']/1000,
    //    desktop['loadingExperience']['metrics']['CUMULATIVE_LAYOUT_SHIFT_SCORE']['category'],
        desktop['loadingExperience']['metrics']['CUMULATIVE_LAYOUT_SHIFT_SCORE']['percentile'], // CLS is unitlessvalues
    //    desktop['loadingExperience']['metrics']['LARGEST_CONTENTFUL_PAINT_MS']['category'],
        desktop['loadingExperience']['metrics']['LARGEST_CONTENTFUL_PAINT_MS']['percentile']/1000,
        desktop['lighthouseResult']['categories']['performance']['score'] * 100,
        desktop['lighthouseResult']['categories']['accessibility']['score'] * 100,
        desktop['lighthouseResult']['categories']['best-practices']['score'] * 100,
        desktop['lighthouseResult']['categories']['seo']['score'] * 100,
        Utilities.formatDate(newAuditDate, 'GMT', 'yyyy-MM-dd')
    ]);
    } else {
        sheet.appendRow([
        Utilities.formatDate(new Date(), 'GMT', 'yyyy-MM-dd'),                   
        pageSpeedMonitorWebsites[i],             
        pageSpeedMonitorPages[i],
        pageSpeedMonitorUrls[i],
        'Desktop',        
        desktop['lighthouseResult']['audits']['metrics']['details']['items'][0]['speedIndex']/1000,
        desktop['lighthouseResult']['audits']['metrics']['details']['items'][0]['firstContentfulPaint']/1000,                   
        desktop['lighthouseResult']['audits']['metrics']['details']['items'][0]['firstMeaningfulPaint']/1000,                   
        desktop['lighthouseResult']['audits']['metrics']['details']['items'][0]['interactive']/1000,                   
        desktop['lighthouseResult']['audits']['metrics']['details']['items'][0]['firstCPUIdle']/1000,
        desktop['lighthouseResult']['audits']['metrics']['details']['items'][0]['estimatedInputLatency']/1000,
        desktop['lighthouseResult']['audits']['server-response-time']['numericValue'].toFixed(0)/1000,
        '',
        '',
        '',
        desktop['lighthouseResult']['categories']['performance']['score'] * 100,
        desktop['lighthouseResult']['categories']['accessibility']['score'] * 100,
        desktop['lighthouseResult']['categories']['best-practices']['score'] * 100,
        desktop['lighthouseResult']['categories']['seo']['score'] * 100,
        Utilities.formatDate(newAuditDate, 'GMT', 'yyyy-MM-dd')
        ]);
    }
    
    if (mobile['loadingExperience'] !== undefined && mobile['loadingExperience']['metrics']) {
    sheet.appendRow([
        Utilities.formatDate(new Date(), 'GMT', 'yyyy-MM-dd'),                   
        pageSpeedMonitorWebsites[i],             
        pageSpeedMonitorPages[i],
        pageSpeedMonitorUrls[i],    
        'Mobile',                   
        mobile['lighthouseResult']['audits']['metrics']['details']['items'][0]['speedIndex']/1000,
        mobile['lighthouseResult']['audits']['metrics']['details']['items'][0]['firstContentfulPaint']/1000,                   
        mobile['lighthouseResult']['audits']['metrics']['details']['items'][0]['interactive']/1000,
        mobile['lighthouseResult']['audits']['metrics']['details']['items'][0]['firstMeaningfulPaint']/1000,
        mobile['lighthouseResult']['audits']['metrics']['details']['items'][0]['firstCPUIdle']/1000,
        mobile['lighthouseResult']['audits']['metrics']['details']['items'][0]['estimatedInputLatency']/1000,
        mobile['lighthouseResult']['audits']['server-response-time']['numericValue'].toFixed(0)/1000,
    //    mobile['loadingExperience']['metrics']['FIRST_INPUT_DELAY_MS']['category'],
        mobile['loadingExperience']['metrics']['FIRST_INPUT_DELAY_MS']['percentile']/1000,
    //    mobile['loadingExperience']['metrics']['CUMULATIVE_LAYOUT_SHIFT_SCORE']['category'],
        mobile['loadingExperience']['metrics']['CUMULATIVE_LAYOUT_SHIFT_SCORE']['percentile'],
    //    mobile['loadingExperience']['metrics']['LARGEST_CONTENTFUL_PAINT_MS']['category'],
        mobile['loadingExperience']['metrics']['LARGEST_CONTENTFUL_PAINT_MS']['percentile']/1000,
        mobile['lighthouseResult']['categories']['performance']['score'] * 100,
        mobile['lighthouseResult']['categories']['accessibility']['score'] * 100,
        mobile['lighthouseResult']['categories']['best-practices']['score'] * 100,
        mobile['lighthouseResult']['categories']['seo']['score'] * 100,
        Utilities.formatDate(newAuditDate, 'GMT', 'yyyy-MM-dd')
    ]);  
    } else {
        sheet.appendRow([
        Utilities.formatDate(new Date(), 'GMT', 'yyyy-MM-dd'),                   
        pageSpeedMonitorWebsites[i],             
        pageSpeedMonitorPages[i],
        pageSpeedMonitorUrls[i],    
        'Mobile',                   
        mobile['lighthouseResult']['audits']['metrics']['details']['items'][0]['speedIndex']/1000,
        mobile['lighthouseResult']['audits']['metrics']['details']['items'][0]['firstContentfulPaint']/1000,                   
        mobile['lighthouseResult']['audits']['metrics']['details']['items'][0]['interactive']/1000,
        mobile['lighthouseResult']['audits']['metrics']['details']['items'][0]['firstMeaningfulPaint']/1000,
        mobile['lighthouseResult']['audits']['metrics']['details']['items'][0]['firstCPUIdle']/1000,
        mobile['lighthouseResult']['audits']['metrics']['details']['items'][0]['estimatedInputLatency']/1000,
        mobile['lighthouseResult']['audits']['server-response-time']['numericValue'].toFixed(0)/1000,
        '',
        '',
        '',
        mobile['lighthouseResult']['categories']['performance']['score'] * 100,
        mobile['lighthouseResult']['categories']['accessibility']['score'] * 100,
        mobile['lighthouseResult']['categories']['best-practices']['score'] * 100,
        mobile['lighthouseResult']['categories']['seo']['score'] * 100,
        Utilities.formatDate(newAuditDate, 'GMT', 'yyyy-MM-dd')
    ]);
    }
    
    for(var j = 0; j < performanceOpportunities.length; j++) {
        if(desktop['lighthouseResult']['audits'][performanceOpportunities[j]]['score'] < 1) {
    //      console.log("klkl");
        oSheet.appendRow([      
            Utilities.formatDate(new Date(), 'GMT', 'yyyy-MM-dd'),                   
            pageSpeedMonitorWebsites[i],             
            pageSpeedMonitorPages[i],
            pageSpeedMonitorUrls[i],    
            'Desktop',      
            'Performance',
            desktop['lighthouseResult']['audits'][performanceOpportunities[j]]['title'],   
            desktop['lighthouseResult']['audits'][performanceOpportunities[j]]['score'],      
        ]); 
        }
            
        if(mobile['lighthouseResult']['audits'][performanceOpportunities[j]]['score'] < 1) {  
        oSheet.appendRow([      
            Utilities.formatDate(new Date(), 'GMT', 'yyyy-MM-dd'),                   
            pageSpeedMonitorWebsites[i],             
            pageSpeedMonitorPages[i],
            pageSpeedMonitorUrls[i],    
            'Mobile',      
            'Performance',
            mobile['lighthouseResult']['audits'][performanceOpportunities[j]]['title'],   
            mobile['lighthouseResult']['audits'][performanceOpportunities[j]]['score'],      
        ]);
        }
    }
            
    //  throw new Error("Something went badly wrong!");     
    for(var j = 0; j < seoOpportunities.length; j++) {
        if(desktop['lighthouseResult']['audits'][seoOpportunities[j]]['score'] < 1) {
        oSheet.appendRow([      
            Utilities.formatDate(new Date(), 'GMT', 'yyyy-MM-dd'),                   
            pageSpeedMonitorWebsites[i],             
            pageSpeedMonitorPages[i],
            pageSpeedMonitorUrls[i],    
            'Desktop',      
            'Seo',
            desktop['lighthouseResult']['audits'][seoOpportunities[j]]['title'],   
            desktop['lighthouseResult']['audits'][seoOpportunities[j]]['score'],      
        ]); 
        }

        if(mobile['lighthouseResult']['audits'][seoOpportunities[j]]['score'] < 1) {    
        oSheet.appendRow([      
            Utilities.formatDate(new Date(), 'GMT', 'yyyy-MM-dd'),                        
            pageSpeedMonitorWebsites[i],             
            pageSpeedMonitorPages[i],
            pageSpeedMonitorUrls[i],    
            'Mobile',      
            'Seo',
            mobile['lighthouseResult']['audits'][seoOpportunities[j]]['title'],   
            mobile['lighthouseResult']['audits'][seoOpportunities[j]]['score'],      
        ]);
        }
    }

    //   throw new Error("Something went badly wrong!"); 
    for(var j = 0; j < accOpportunities.length; j++) {
        if (desktop['lighthouseResult']['audits'][accOpportunities[j]]['score'] < 1) {  
        oSheet.appendRow([      
            Utilities.formatDate(new Date(), 'GMT', 'yyyy-MM-dd'),                   
            pageSpeedMonitorWebsites[i],             
            pageSpeedMonitorPages[i],
            pageSpeedMonitorUrls[i],    
            'Desktop',      
            'Accessibility',
            desktop['lighthouseResult']['audits'][accOpportunities[j]]['title'],   
            desktop['lighthouseResult']['audits'][accOpportunities[j]]['score'],      
        ]); 
        }
        if(mobile['lighthouseResult']['audits'][accOpportunities[j]]['score'] < 1) {  
        oSheet.appendRow([      
            Utilities.formatDate(new Date(), 'GMT', 'yyyy-MM-dd'),                   
            pageSpeedMonitorWebsites[i],             
            pageSpeedMonitorPages[i],
            pageSpeedMonitorUrls[i],    
            'Mobile',      
            'Accessibility',
            mobile['lighthouseResult']['audits'][accOpportunities[j]]['title'],   
            mobile['lighthouseResult']['audits'][accOpportunities[j]]['score'],      
        ]);
        }
    }

    //        throw new Error("Something went badly wrong!");
    for(var j = 0; j < bestPractOpportunities.length; j++) {
        if(desktop['lighthouseResult']['audits'][bestPractOpportunities[j]]['score'] < 1) {
        oSheet.appendRow([      
            Utilities.formatDate(new Date(), 'GMT', 'yyyy-MM-dd'),                      
            pageSpeedMonitorWebsites[i],             
            pageSpeedMonitorPages[i],
            pageSpeedMonitorUrls[i],    
            'Desktop',      
            'Best-practices',
            desktop['lighthouseResult']['audits'][bestPractOpportunities[j]]['title'],   
            desktop['lighthouseResult']['audits'][bestPractOpportunities[j]]['score'],      
        ]); 
        }

        if(mobile['lighthouseResult']['audits'][bestPractOpportunities[j]]['score'] < 1) {       
        oSheet.appendRow([      
            Utilities.formatDate(new Date(), 'GMT', 'yyyy-MM-dd'),                   
            pageSpeedMonitorWebsites[i],             
            pageSpeedMonitorPages[i],
            pageSpeedMonitorUrls[i],    
            'Mobile',      
            'Best-practices',
            mobile['lighthouseResult']['audits'][bestPractOpportunities[j]]['title'],   
            mobile['lighthouseResult']['audits'][bestPractOpportunities[j]]['score'],      
        ]);
        }
    }

  } catch(err) {
        var ssDelete = SpreadsheetApp.getActiveSpreadsheet().getSheetByName(client+' - Results'); //fetch result files based on executed function
        // This logs the value in the very last cell of this sheet
        var lastRow = ssDelete.getLastRow();
        var values = ssDelete.getSheetValues(lastRow, 1, 1, 5); //lastrow first 5 values 
//        console.log("Last Row to delete " + lastRow + " for "+ pageSpeedMonitorUrls[i]);
//        console.log("Values " + values + " for "+ pageSpeedMonitorUrls[i]);
        if (values[0][1] === pageSpeedMonitorWebsites[i] && 
            values[0][2] === pageSpeedMonitorPages[i] &&  
              values[0][3] === pageSpeedMonitorUrls[i] &&   
                values[0][4] === 'Desktop') {
                  console.log("Last Deleted Row was " + lastRow + " for "+ pageSpeedMonitorUrls[i]);
                  ssDelete.deleteRow(lastRow);
                }
        var failedScriptError = "Error has occured at " + pageSpeedMonitorUrls[i] + " Error Detail: " + err;
          console.log(failedScriptError);
        failedScripts.push(failedScriptError);
        failedValues.push(i);
        return;
    //   if (count > 5) {
    //     var message = pageSpeedMonitorUrls[i]+" has failed 5 times to fetch values because "+ err;
    //     failed.push(message);
    //     count = 1;
    //     return;
    //   } else {
    //     var failedScript = "Error has occured at " + pageSpeedMonitorUrls[i] + " Error Detail: " + err;
    //     console.log("Count " +count);
    //     console.log(failedScript);
    //     count++;
    //     appendRows(i);
    // }
  }
}

function testUrlsAgain() {
  count++;
  console.log(count);
  var testfailedScripts = [];
  var test = [];
  testfailedScripts = failedScripts;
  test = failedValues;
  failedScripts = [];
  failedValues = [];
  for (var k = 0; k < test.length; k++) {
    appendRows(test[k]);
  }

  if (count >= 5) {
    failedScriptMessages = testValues;
    // console.log(`this url named ${pageSpeedMonitorUrls[]}`)
    return;
  } else {
    if (failedValues.length > 0) {
      testUrlsAgain();
    }
  }
}

function callPageSpeed(strategy, url) {
  //var pageSpeedUrl = 'https://www.googleapis.com/pagespeedonline/v5/runPagespeed?url=' + url + '&key=' + pageSpeedApiKey + '&category=seo' +'&strategy=' + strategy;
  var pageSpeedUrl = 'https://www.googleapis.com/pagespeedonline/v5/runPagespeed?url=' + url + '&key=' + pageSpeedApiKey + '&category=performance' + '&category=seo' + '&category=accessibility' + '&category=best-practices' +'&strategy=' + strategy;
//  var pageSpeedUrl = 'https://www.googleapis.com/pagespeedonline/v5/runPagespeed?url=' + url + '&key=' + pageSpeedApiKey + '&category=performance&strategy=' + strategy;               
  
                 options = {muteHttpExceptions: true};
  var response = UrlFetchApp.fetch(pageSpeedUrl, options);
  var json = response.getContentText();

  if(response.getResponseCode() == "200") {
    return JSON.parse(json);   
  } else {
    return response.getResponseCode();
    MailApp.sendEmail("nikhils@prdxn.com",                                         
                  "Page Speed App - Problem URL",
                  "Can`t get Page Speed for"+ url +". Response from server is" + response.getResponseCode());
  }    
}