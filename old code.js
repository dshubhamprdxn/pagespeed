var pageSpeedApiKey = 'AIzaSyDqEGvYdQbMC_i7ihHlJYtr5yUSdJ5ePfY';
var desktop,mobile,spreadsheet,sheet, urlSpreadSheet, urlSheet, urls;
var pageSpeedMonitorUrls = [];
var pageSpeedMonitorPages = [];
var pageSpeedMonitorWebsites = [];
var performanceOpportunities = ['efficient-animated-content', 
                                'render-blocking-resources',
                                'uses-optimized-images',
                                'uses-text-compression',
                                'uses-rel-preconnect',
                                'unminified-css',
                                'offscreen-images',
                                'uses-responsive-images',
                                'unused-css-rules',
                                'uses-webp-images',
                                'uses-rel-preload',
                                'unminified-javascript',
                                'redirects',                                
                               ];
var seoOpportunities = ['document-title',
                        'robots-txt', 
                        'link-text', 
                        'http-status-code',
                        'image-alt', 
                        'plugins',
                        'meta-description',
                        'viewport', 
                        'canonical', 
                        'hreflang', 
                        'is-crawlable'                                                                         
                       ]; 

var accOpportunities = ['aria-allowed-attr',
                        'button-name', 
                        'button-name', 
                        'color-contrast', 
                        'aria-required-parent', 
                        'bypass', 
                        'image-alt',
                        'tabindex',
                        'html-has-lang',
                        'link-name',
                        'duplicate-id',
                        'meta-viewport'
                       ];

var bestPractOpportunities = ['appcache-manifest',
                              'external-anchors-use-rel-noopener',
                              'deprecations',
                              'no-vulnerable-libraries',
                              'password-inputs-can-be-pasted-into',
                              'doctype',
                              'errors-in-console',
                              'js-libraries',
                              'geolocation-on-start',
                              'notification-on-start',
                              'is-on-https',
                              'no-document-write',
                              'image-aspect-ratio',
                              'uses-passive-event-listeners'

]

function monitor() {
  urlSpreadSheet = SpreadsheetApp.getActiveSpreadsheet();
  urlSheet = urlSpreadSheet.getSheetByName('Site Urls');
  urls = urlSheet.getDataRange().getValues();
  
  function addDays(date, days) {
    const copy = new Date(Number(date))
    copy.setDate(date.getDate() + days)
    return copy;
  }

  const date = new Date();
  const newAuditDate = addDays(date, 14);
  
//  console.log(Utilities.formatDate(newAuditDate, 'GMT', 'yyyy-MM-dd'));  
  for (var i = 1; i < urls.length; i++) {
    pageSpeedMonitorUrls.push(urls[i][0]);
    pageSpeedMonitorPages.push(urls[i][1]);
    pageSpeedMonitorWebsites.push(urls[i][2]); 
  }        
  
  for (var i = 0; i < pageSpeedMonitorUrls.length; i++) {          
    desktop = callPageSpeed('desktop', pageSpeedMonitorUrls[i]);
    
//console.log(desktop['lighthouseResult']);
   
//        throw new Error("Something went badly wrong!");
    //['categories']['performance']);        
    
    mobile = callPageSpeed('mobile', pageSpeedMonitorUrls[i]);
    
    if (Object.keys(desktop).length === 0 && Object.keys(mobile).length === 0 ) {
      continue;
    }
    
    spreadsheet = SpreadsheetApp.getActiveSpreadsheet();
    //    sheet = spreadsheet.getSheetByName('Results'); //doublecheck your sheet's name!s                              
    sheet = spreadsheet.getSheetByName('testing(Dev)');
    
    console.log(desktop['lighthouseResult']['categories']['performance']['score'] * 100);
    console.log(desktop['lighthouseResult']['categories']['accessibility']['score'] * 100);
    console.log(desktop['lighthouseResult']['categories']['best-practices']['score'] * 100);
    
    sheet.appendRow([
      Utilities.formatDate(new Date(), 'GMT', 'yyyy-MM-dd'),                   
      pageSpeedMonitorWebsites[i],             
      pageSpeedMonitorPages[i],
      pageSpeedMonitorUrls[i],
      'Desktop',        
      desktop['lighthouseResult']['audits']['metrics']['details']['items'][0]['speedIndex']/1000,
    desktop['lighthouseResult']['audits']['metrics']['details']['items'][0]['firstContentfulPaint']/1000,                   
    desktop['lighthouseResult']['audits']['metrics']['details']['items'][0]['firstMeaningfulPaint']/1000,                   
    desktop['lighthouseResult']['audits']['metrics']['details']['items'][0]['interactive']/1000,                   
    desktop['lighthouseResult']['audits']['metrics']['details']['items'][0]['firstCPUIdle']/1000,
    desktop['lighthouseResult']['audits']['metrics']['details']['items'][0]['estimatedInputLatency']/1000,
    //                   desktop['lighthouseResult']['audits']['time-to-first-byte']['numericValue'].toFixed(0)/1000,
    'ddd',
    desktop['lighthouseResult']['categories']['performance']['score'] * 100,
    desktop['lighthouseResult']['categories']['accessibility']['score'] * 100,
    desktop['lighthouseResult']['categories']['best-practices']['score'] * 100,
    desktop['lighthouseResult']['categories']['seo']['score'] * 100,
    Utilities.formatDate(newAuditDate, 'GMT', 'yyyy-MM-dd'),
    desktop['loadingExperience']['metrics']['FIRST_INPUT_DELAY_MS']['category'],
    desktop['loadingExperience']['metrics']['CUMULATIVE_LAYOUT_SHIFT_SCORE']['percentile'],
    //        desktop['loadingExperience']['metrics']['LARGEST_CONTENTFUL_PAINT_MS']['category'],
    //      desktop['lighthouseResult']['audits']['cumulative-layout-shift']['score'], //for cumulative layout shift
    //      desktop['lighthouseResult']['audits']['largest-contentful-paint']['displayValue'] //for largest-contentful-paint
    ]);
    sheet.appendRow([
                   Utilities.formatDate(new Date(), 'GMT', 'yyyy-MM-dd'),                   
                   pageSpeedMonitorWebsites[i],             
                   pageSpeedMonitorPages[i],
                   pageSpeedMonitorUrls[i],    
                   'Mobile',                   
                   mobile['lighthouseResult']['audits']['metrics']['details']['items'][0]['speedIndex']/1000,
                   mobile['lighthouseResult']['audits']['metrics']['details']['items'][0]['firstContentfulPaint']/1000,                   
                   mobile['lighthouseResult']['audits']['metrics']['details']['items'][0]['interactive']/1000,
                   mobile['lighthouseResult']['audits']['metrics']['details']['items'][0]['firstMeaningfulPaint']/1000,
                   mobile['lighthouseResult']['audits']['metrics']['details']['items'][0]['firstCPUIdle']/1000,
                   mobile['lighthouseResult']['audits']['metrics']['details']['items'][0]['estimatedInputLatency']/1000,
//                   mobile['lighthouseResult']['audits']['time-to-first-byte']['numericValue'].toFixed(0)/1000,
                    'ddd',
                   mobile['lighthouseResult']['categories']['performance']['score'] * 100,
                   mobile['lighthouseResult']['categories']['accessibility']['score'] * 100,
                   mobile['lighthouseResult']['categories']['best-practices']['score'] * 100,
                   mobile['lighthouseResult']['categories']['seo']['score'] * 100,    
                   Utilities.formatDate(newAuditDate, 'GMT', 'yyyy-MM-dd'),
        mobile['loadingExperience']['metrics']['FIRST_INPUT_DELAY_MS']['category'],
        mobile['loadingExperience']['metrics']['CUMULATIVE_LAYOUT_SHIFT_SCORE']['percentile'],
//        mobile['loadingExperience']['metrics']['LARGEST_CONTENTFUL_PAINT_MS']['category'],
//      mobile['lighthouseResult']['audits']['cumulative-layout-shift']['score'], //for cumulative layout shift
//      mobile['lighthouseResult']['audits']['largest-contentful-paint']['displayValue'] //for largest-contentful-paint
                  ]);    
  
  
    throw new Error("Something went badly wrong!");
    for(var j = 0; j < performanceOpportunities.length; j++) {
      oppSheet = spreadsheet.getSheetByName('Opportunities');
//      console.log(desktop['lighthouseResult']['audits'][performanceOpportunities[j]]['score'] < 1);
      if(desktop['lighthouseResult']['audits'][performanceOpportunities[j]]['score'] < 1) {
        console.log("klkl");
      oppSheet.appendRow([      
                 Utilities.formatDate(new Date(), 'GMT', 'yyyy-MM-dd'),                   
                 pageSpeedMonitorWebsites[i],             
                 pageSpeedMonitorPages[i],
                 pageSpeedMonitorUrls[i],    
                 'Desktop',      
                 'Performance',
                 desktop['lighthouseResult']['audits'][performanceOpportunities[j]]['title'],   
                 desktop['lighthouseResult']['audits'][performanceOpportunities[j]]['score'],      
    ]); 
        }
          
      if(mobile['lighthouseResult']['audits'][performanceOpportunities[j]]['score'] < 1) {  
      oppSheet.appendRow([      
                 Utilities.formatDate(new Date(), 'GMT', 'yyyy-MM-dd'),                   
                 pageSpeedMonitorWebsites[i],             
                 pageSpeedMonitorPages[i],
                 pageSpeedMonitorUrls[i],    
                 'Mobile',      
                 'Performance',
                 mobile['lighthouseResult']['audits'][performanceOpportunities[j]]['title'],   
                 mobile['lighthouseResult']['audits'][performanceOpportunities[j]]['score'],      
    ]);
      }
    }
        
//        throw new Error("Something went badly wrong!"); 
    
    for(var j = 0; j < seoOpportunities.length; j++) {
        if(desktop['lighthouseResult']['audits'][seoOpportunities[j]]['score'] < 1) {
              oppSheet.appendRow([      
                 Utilities.formatDate(new Date(), 'GMT', 'yyyy-MM-dd'),                   
                 pageSpeedMonitorWebsites[i],             
                 pageSpeedMonitorPages[i],
                 pageSpeedMonitorUrls[i],    
                 'Desktop',      
                 'Seo',
                 desktop['lighthouseResult']['audits'][seoOpportunities[j]]['title'],   
                 desktop['lighthouseResult']['audits'][seoOpportunities[j]]['score'],      
               ]); 
          }
        
       if(mobile['lighthouseResult']['audits'][seoOpportunities[j]]['score'] < 1) {
                
          oppSheet.appendRow([      
                 Utilities.formatDate(new Date(), 'GMT', 'yyyy-MM-dd'),                        
                 pageSpeedMonitorWebsites[i],             
                 pageSpeedMonitorPages[i],
                 pageSpeedMonitorUrls[i],    
                 'Mobile',      
                 'Seo',
                 mobile['lighthouseResult']['audits'][seoOpportunities[j]]['title'],   
                 mobile['lighthouseResult']['audits'][seoOpportunities[j]]['score'],      
    ]);
            }
    }
    
//    for(var j = 0; j < accOpportunities.length; j++) {
//            if (desktop['lighthouseResult']['audits'][accOpportunities[j]]['score'] < 1) {  
//               oppSheet.appendRow([      
//                  Utilities.formatDate(new Date(), 'GMT', 'yyyy-MM-dd'),                   
//                  pageSpeedMonitorWebsites[i],             
//                  pageSpeedMonitorPages[i],
//                  pageSpeedMonitorUrls[i],    
//                  'Desktop',      
//                  'Accessibility',
//                  desktop['lighthouseResult']['audits'][accOpportunities[j]]['title'],   
//                  desktop['lighthouseResult']['audits'][accOpportunities[j]]['score'],      
//                ]); 
//              }
//            if(mobile['lighthouseResult']['audits'][accOpportunities[j]]['score'] < 1) {  
//                oppSheet.appendRow([      
//                 Utilities.formatDate(new Date(), 'GMT', 'yyyy-MM-dd'),                   
//                 pageSpeedMonitorWebsites[i],             
//                 pageSpeedMonitorPages[i],
//                 pageSpeedMonitorUrls[i],    
//                 'Mobile',      
//                 'Accessibility',
//                 mobile['lighthouseResult']['audits'][accOpportunities[j]]['title'],   
//                 mobile['lighthouseResult']['audits'][accOpportunities[j]]['score'],      
//              ]);
//            }
//       
//    }
    
    for(var j = 0; j < bestPractOpportunities.length; j++) {
      
       if(desktop['lighthouseResult']['audits'][bestPractOpportunities[j]]['score'] < 1) {
       
           oppSheet.appendRow([      
                 Utilities.formatDate(new Date(), 'GMT', 'yyyy-MM-dd'),                      
                 pageSpeedMonitorWebsites[i],             
                 pageSpeedMonitorPages[i],
                 pageSpeedMonitorUrls[i],    
                 'Desktop',      
                 'Best-practices',
                 desktop['lighthouseResult']['audits'][bestPractOpportunities[j]]['title'],   
                 desktop['lighthouseResult']['audits'][bestPractOpportunities[j]]['score'],      
               ]); 
       }
             
       if(mobile['lighthouseResult']['audits'][bestPractOpportunities[j]]['score'] < 1) {       
               oppSheet.appendRow([      
                 Utilities.formatDate(new Date(), 'GMT', 'yyyy-MM-dd'),                   
                 pageSpeedMonitorWebsites[i],             
                 pageSpeedMonitorPages[i],
                 pageSpeedMonitorUrls[i],    
                 'Mobile',      
                 'Best-practices',
                 mobile['lighthouseResult']['audits'][bestPractOpportunities[j]]['title'],   
                 mobile['lighthouseResult']['audits'][bestPractOpportunities[j]]['score'],      
                ]);
             }
    }
    
  }
                                  
}


function callPageSpeed(strategy, url) {
  //var pageSpeedUrl = 'https://www.googleapis.com/pagespeedonline/v5/runPagespeed?url=' + url + '&key=' + pageSpeedApiKey + '&category=seo' +'&strategy=' + strategy;
//  var pageSpeedUrl = 'https://www.googleapis.com/pagespeedonline/v5/runPagespeed?url=' + url + '&key=' + pageSpeedApiKey + '&category=performance' + '&category=seo' + '&categorwy=accessibility' + '&category=best-practices' +'&strategy=' + strategy;
  var pageSpeedUrl = 'https://www.googleapis.com/pagespeedonline/v5/runPagespeed?url=' + url + '&key=' + pageSpeedApiKey + '&category=performance' + '&category=seo' + '&category=accessibility' + '&category=best-practices' +'&strategy=' + strategy;


                 options = {muteHttpExceptions: true};
  var response = UrlFetchApp.fetch(pageSpeedUrl, options);
  var json = response.getContentText();

  if(response.getResponseCode() == "200") {
    return JSON.parse(json);   
  } else {
    return response.getResponseCode();
    MailApp.sendEmail("nikhils@prdxn.com",                                         
                  "Page Speed App - Problem URL",
                  "Can`t get Page Speed for"+ url +". Response from server is" + response.getResponseCode());
  }    
}